# Alien Invasion

**In this game waves of aliens descend on you from above. Shoot'em up using you battle spaceship guns to protect Earth and all the beauty in the world.**

*To launch the game you need execute "alien_invasion.py" via Python 3 with PyGame module installed.*

In game you have 3 ships. When alien touches you or reaches ground you loosing 1 ship. When ships over -- game ends.
Use spacebar to shoot and arrow keys to move left and right.

![alt text](alien_invasion_gameplay.gif)

_This game was developed by me as a part of Python practice and mastering using guide from E. Mattes book "Python Crash-course"._
