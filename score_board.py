import pygame.font
from pygame.sprite import Group

from ship import Ship
from game_stats import GameStats


class Scoreboard:
	def __init__(self, ai_game):
		self.screen = ai_game.screen 
		self.screen_rect = self.screen.get_rect()
		self.settings = ai_game.settings
		self.stats = ai_game.stats

		self.ai_game = ai_game

		self.text_color = (0,0,0)
		self.font = pygame.font.SysFont(None, 48)

		self.prep_score()
		self.prep_highscore()
		self.prep_level()
		self.prep_ships()

	def prep_ships(self):

		self.ships = Group()
		for ship_number in range(self.stats.ships_left):
			ship = Ship(self.ai_game)
			ship.rect.x = 10 + ship_number * ship.rect.width
			ship.rect.y = 10
			self.ships.add(ship)

	def prep_score(self):
		rounded_score = round(self.stats.score, -1)
		self.score_str = "{:,}".format(rounded_score)
		self.score_img = self.font.render(self.score_str, True, self.text_color)

		self.score_rect = self.score_img.get_rect()
		self.score_rect.right = self.screen_rect.right - 25
		self.score_rect.top = 20

	def prep_highscore(self):
		high_score = round(self.stats.high_score, -1)
		high_score_str = "{:,}".format(high_score)
		self.high_score_img = self.font.render(high_score_str, True, self.text_color)

		self.high_score_rect = self.high_score_img.get_rect()
		self.high_score_rect.centerx = self.screen_rect.centerx
		self.high_score_rect.top = self.score_rect.top

	def prep_level(self):
		level_str = str(self.stats.current_level)
		self.level_img = self.font.render(level_str, True, self.text_color)

		self.level_rect = self.level_img.get_rect()
		self.level_rect.right = self.score_rect.right
		self.level_rect.top = self.score_rect.bottom + 10

	def check_highscore(self):
		if self.stats.score > self.stats.high_score:
			self.stats.high_score = self.stats.score
			self.prep_highscore()


	def show_score(self):
		self.screen.blit(self.score_img, self.score_rect)
		self.screen.blit(self.high_score_img, self.high_score_rect)
		self.screen.blit(self.level_img, self.level_rect)
		self.ships.draw(self.screen)